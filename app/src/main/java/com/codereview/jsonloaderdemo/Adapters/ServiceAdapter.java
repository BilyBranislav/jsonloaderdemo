package com.codereview.jsonloaderdemo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codereview.jsonloaderdemo.R;
import com.codereview.jsonloaderdemo.Service;

import java.util.ArrayList;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceHolder>{

    private ArrayList<Service> services;
    private OnServiceClickListener onServiceClickListener;

    private final String TAG = "ServiceAdapter";

    public ServiceAdapter(OnServiceClickListener onServiceClickListener, ArrayList<Service> services) {
        super();
        this.services = services;
        this.onServiceClickListener = onServiceClickListener;
    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.service_holder, viewGroup, false);
        return new ServiceHolder(v, onServiceClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHolder serviceHolder, int i) {
        serviceHolder.setData(services.get(i));
    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    class ServiceHolder extends RecyclerView.ViewHolder {

        private OnServiceClickListener onServiceClickListener;
        private View itemView;

        private TextView id;
        private TextView name;
        private TextView lastName;

        public ServiceHolder(@NonNull View itemView, OnServiceClickListener onServiceClickListener) {
            super(itemView);
            this.itemView = itemView;
            this.onServiceClickListener = onServiceClickListener;
        }

        void setData(final Service service) {
            id = itemView.findViewById(R.id.serviceID);
            name = itemView.findViewById(R.id.serviceName);
            lastName = itemView.findViewById(R.id.serviceLastName);
            id.setText(service.getOwnerID());
            name.setText(service.getName());
            lastName.setText(service.getName());
            Log.i(TAG, service.toString());

            itemView.setOnClickListener(v -> onServiceClickListener.onServiceClick(Integer.parseInt(service.getOwnerID())));
        }
    }

    public interface OnServiceClickListener {
        void onServiceClick(int serviceID);
    }
}

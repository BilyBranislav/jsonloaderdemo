package com.codereview.jsonloaderdemo;

import com.google.gson.annotations.SerializedName;

public class Service {

    @SerializedName("obchodné_meno")
    public String name;

    @SerializedName("prevádzka")
    public String description;

    @SerializedName("činnosť")
    public String activity;

    @SerializedName("ulica")
    public String street;

    public String Sup_c;

    public String Or_c;

    @SerializedName("obec")
    public String municipality;

    @SerializedName("adresa_prevádzky")
    public String location;

    @SerializedName("ičo")
    public String ownerID;

    @SerializedName("dátum_začatia_činnosti")
    public String runningSince;

    @SerializedName("prevádzková_doba_pondelok")
    public String opennedOnMonday;

    @SerializedName("prevádzková_doba_utorok")
    public String opennedOnTuesday;

    @SerializedName("prevádzková_doba_streda")
    public String opennedOnWednesday;

    @SerializedName("prevádzková_doba_štvrtok")
    public String opennedOnThursday;

    @SerializedName("prevádzková_doba_piatok")
    public String opennedOnFriday;

    @SerializedName("prevádzková_doba_sobota")
    public String opennedOnSaturday;

    @SerializedName("prevádzková_doba_nedeľa")
    public String opennedOnSunday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSup_c() {
        return Sup_c;
    }

    public void setSup_c(String sup_c) {
        Sup_c = sup_c;
    }

    public String getOr_c() {
        return Or_c;
    }

    public void setOr_c(String or_c) {
        Or_c = or_c;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getRunningSince() {
        return runningSince;
    }

    public void setRunningSince(String runningSince) {
        this.runningSince = runningSince;
    }

    public String getOpennedOnMonday() {
        return opennedOnMonday;
    }

    public void setOpennedOnMonday(String opennedOnMonday) {
        this.opennedOnMonday = opennedOnMonday;
    }

    public String getOpennedOnTuesday() {
        return opennedOnTuesday;
    }

    public void setOpennedOnTuesday(String opennedOnTuesday) {
        this.opennedOnTuesday = opennedOnTuesday;
    }

    public String getOpennedOnWednesday() {
        return opennedOnWednesday;
    }

    public void setOpennedOnWednesday(String opennedOnWednesday) {
        this.opennedOnWednesday = opennedOnWednesday;
    }

    public String getOpennedOnThursday() {
        return opennedOnThursday;
    }

    public void setOpennedOnThursday(String opennedOnThursday) {
        this.opennedOnThursday = opennedOnThursday;
    }

    public String getOpennedOnFriday() {
        return opennedOnFriday;
    }

    public void setOpennedOnFriday(String opennedOnFriday) {
        this.opennedOnFriday = opennedOnFriday;
    }

    public String getOpennedOnSaturday() {
        return opennedOnSaturday;
    }

    public void setOpennedOnSaturday(String opennedOnSaturday) {
        this.opennedOnSaturday = opennedOnSaturday;
    }

    public String getOpennedOnSunday() {
        return opennedOnSunday;
    }

    public void setOpennedOnSunday(String opennedOnSunday) {
        this.opennedOnSunday = opennedOnSunday;
    }

    @Override
    public String toString() {
        return "Service{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", activity='" + activity + '\'' +
                ", street='" + street + '\'' +
                ", Sup_c='" + Sup_c + '\'' +
                ", Or_c='" + Or_c + '\'' +
                ", municipality='" + municipality + '\'' +
                ", location='" + location + '\'' +
                ", ownerID='" + ownerID + '\'' +
                ", runningSince='" + runningSince + '\'' +
                ", opennedOnMonday='" + opennedOnMonday + '\'' +
                ", opennedOnTuesday='" + opennedOnTuesday + '\'' +
                ", opennedOnWednesday='" + opennedOnWednesday + '\'' +
                ", opennedOnThursday='" + opennedOnThursday + '\'' +
                ", opennedOnFriday='" + opennedOnFriday + '\'' +
                ", opennedOnSaturday='" + opennedOnSaturday + '\'' +
                ", opennedOnSunday='" + opennedOnSunday + '\'' +
                '}';
    }
}

package com.codereview.jsonloaderdemo

data class Trying(
        val id: Int = 0,
        val name: String? = null,
        val lastName: String? = null
)
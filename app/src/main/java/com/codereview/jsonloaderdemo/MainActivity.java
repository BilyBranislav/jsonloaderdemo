package com.codereview.jsonloaderdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.codereview.jsonloaderdemo.Adapters.ServiceAdapter;
import com.codereview.jsonloaderdemo.JSONDownload.JSONDownload;
import com.codereview.jsonloaderdemo.JSONParser.JSONParser;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements ServiceAdapter.OnServiceClickListener {

    private ProgressBar progressBar;

    private final String url = "http://popmyshop.azurewebsites.net/api/services/Pir";
    private static String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBarCircular);
        loadRecyclerView();
    }

    private void loadRecyclerView() {
        //Shows progress bar while application load data
        progressBar.setVisibility(View.VISIBLE);
        //Url that returns JSON
        JSONDownload jsonDownload = new JSONDownload();
        String json = null;
        try {
            json = jsonDownload.execute(url).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //If JSON request was not successful, this occurs mostly when user has no internet access
        if(json == null) {
            Toast.makeText(this, "Check your internet access", Toast.LENGTH_SHORT).show();

        } else {
            ArrayList<Service> services = new JSONParser().getObjectFromResult(json);
            Log.i(TAG, "loadRecyclerView: " + services.size());
            setUpRecyclerView(services);
        }
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void setUpRecyclerView(ArrayList<Service> services) {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ServiceAdapter adapter = new ServiceAdapter(this, services);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onServiceClick(int serviceID) {
        Toast.makeText(this, String.valueOf(serviceID), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh: {
                loadRecyclerView();
            }
        }
        return true;
    }


}
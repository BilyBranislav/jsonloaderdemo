package com.codereview.jsonloaderdemo.JSONParser;

import android.util.Log;

import com.codereview.jsonloaderdemo.Service;
import com.codereview.jsonloaderdemo.Trying;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class JSONParser {

    public ArrayList<Service> getObjectFromResult(String result) {
        ArrayList<Service> services = new ArrayList<>();
        try {
            //Read GSON documentation for further detail
            Gson gson = new Gson();
            //Creates ArrayList<Trying> type, that will be used in creating object out of JSON
            Type listType = new TypeToken<ArrayList<Service>>(){}.getType();
            //Fills ArrayList with all the object in JSON
            services = gson.fromJson(result, listType);
            Log.i("Services", services.toString());
        } catch (NullPointerException e) {
            Log.e(TAG, e.toString());
        }
        return services;
    }
}

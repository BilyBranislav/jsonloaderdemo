package com.codereview.jsonloaderdemo.JSONDownload;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class JSONDownload extends AsyncTask<String, Void, String> {

    private static String TAG = "JSONDOWNLOAD";

    @Override
    protected String doInBackground(String... urls) {
        StringBuilder result = new StringBuilder();
        URL url;
        HttpURLConnection urlConnection;
        try {
            //Gets first url of possible many
            url = new URL(urls[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = urlConnection.getInputStream();
            InputStreamReader reader = new InputStreamReader(in);
            int data = reader.read();
            //Reads JSON into a int
            while (data != -1) {
                char current = (char) data;
                result.append(current);
                data = reader.read();
            }
            //returns JSON in String
            Log.i(TAG, "doInBackground: " + result);
            return result.toString();
        } catch (MalformedURLException e) {
            Log.e(TAG, "doInBackground: ", e);
        } catch (IOException e) {
            Log.e(TAG, "doInBackground: ", e);

        }
        return null;
    }
}
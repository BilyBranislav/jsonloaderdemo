package com.codereview.jsonloaderdemo

data class Service (

    var obchodné_meno: String? = null,

    var prevádzka: String? = null,

    var Činnosť: String? = null,

    var ulica: String? = null,

    var sup_c: String? = null,

    var or_c: String? = null,

    var obec: String? = null,

    var adresa_prevádzky: String? = null,

    var iČO: String? = null,

    var dátum_začatia_činnosti: String? = null,

    var prevádzková_doba_pondelok: String? = null,

    var prevádzková_doba_utorok: String? = null,

    var prevádzková_doba_streda: String? = null,

    var prevádzková_doba_štvrtok: String? = null,

    var prevádzková_doba_piatok: String? = null,

    var prevádzková_doba_sobota: String? = null,

    var prevádzková_doba_nedeľa: String? = null
)
